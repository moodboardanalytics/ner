import spacy 
from spacy.matcher import PhraseMatcher
import random
from pathlib import Path

def offseter(lbl, doc, matchitem):
	o_one=len(str(doc[0:matchitem[1]]))
	subdoc=doc[matchitem[1]:matchitem[2]]
	o_two=o_one+len(str(subdoc))
	return (o_one, o_two, lbl)

nlp=spacy.load('en')

if 'ner' not in nlp.pipe_names:
	ner=nlp.create_pipe('ner')
	nlp.add_pipe(ner)
else:
	ner=nlp.get_pipe('ner')


matcher=PhraseMatcher(nlp.vocab)
label='sleevelength'

ner.add_label(label)

for i in ['-short sleeves','-long sleeves', 'Short Sleeve', 'Long Sleeve', 'short sleeve', 'long sleeve','sleeveless','Sleeveless']:
	matcher.add(label, None, nlp(i))

res=[]
to_train_ents=[]

with open('train.txt') as t:
	line=True
	while line:
		line=t.readline()
		doc=nlp(line)
		matches=matcher(doc)
		res=[offseter(label, doc, match) for match in matches]
		to_train_ents.append((line, dict(entities=res)))

optimizer=nlp.begin_training()

other_pipes=[pipe for pipe in nlp.pipe_names if pipe!='ner']

with nlp.disable_pipes(*other_pipes):
	for itn in range(1):
		losses={}
		random.shuffle(to_train_ents)
		for text,annotations in to_train_ents:
			nlp.update([text],[annotations],sgd=optimizer,drop=0.35,losses=losses)
		print(losses)

nlp.to_disk(Path('/home/Desktop'))

sample=nlp("I have one short sleeve and one long sleeve shirt")

for ent in sample.ents:
	print(ent.text, ent.start_char, ent.end_char, ent.label_)'''